/*���������� ������� ��. ���� �� > 4000, 
����� ��� ������� ������� �� ������� �� ����������,  
����� ��� ������� ������� �� ������� �� ���������� = 2000.
*/
drop table Salary;
create table Salary
(
sal number
);
commit;

insert into Salary values(4500);
insert into Salary values(3800);
insert into Salary values(2500);
insert into Salary values(4900);
insert into Salary values(5000);
commit;

SELECT 
 avg(CASE
         WHEN salary >= 4000 THEN
          salary
         ELSE 2000
       END)
  FROM Salary1;
