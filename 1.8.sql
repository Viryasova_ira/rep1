/*�������� ��������� ������������������:
    100
    101
    102
*/
drop table from_to;
create table from_to
(
id number,
from_src number,
to_src number
);
commit;
insert into from_to values(1,100,101);
insert into from_to values(2,100,102);
insert into from_to values(3,100,103);
insert into from_to values(4,101,102);
insert into from_to values(5,101,103);
insert into from_to values(6,102,103);

SELECT Z.Uniq FROM
(SELECT DISTINCT from_src as Uniq FROM from_to 
UNION
SELECT DISTINCT to_src as Uniq FROM from_to) Z order by Uniq
