/*������� �������, ID ������ � �������� ���� �����������, 
�������� ������� ����� ����������� �������� ������-���� ������*/
drop table employees;
create table employees
(
FIO varchar2(70),
Salary number,
Department varchar2(20)
);
commit;
insert into employees values('Ivanov',1200,'Public');
insert into employees values('Petrov',10000,'Puti');
insert into employees values('Vasiliev',1200,'Harik');
insert into employees values('Iliin',2500,'Puma');
insert into employees values('Bedakov',2000,'Public');
insert into employees values('Viryasova',2000,'Puti');
commit;

SELECT FIO,salary
FROM employees 
WHERE Salary IN (select min(salary) from employees group by department);
