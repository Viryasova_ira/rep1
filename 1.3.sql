/*
Замена N-ного символа в строке. Выполните задание с помощью substr  и regexp_replace
   208010100000  -> 2080X0100000
   208010100000 ->  2X8010100000

*/
select regexp_replace('208010100000','.','X', 5, 1) from dual;
select regexp_replace('208010100000','.','X', 2, 1) from dual;
