/*
���� ������� �������� GRATH_SRC � TEMP_GRATH
� ��������� ������� ������ ������� ����� ������, 
��������� � ���������� ������ �� ���� weight_src

*/
create  table grath_src
(
id number,
from_src number,
to_src number,
weight_src number
)
commit;
insert into grath_src values(1,100,102,100);
insert into grath_src values(2,100,101,400);
insert into grath_src values(3,101,102,200);
insert into grath_src values(4,300,301,300);
insert into grath_src values(5,300,302,400);
insert into grath_src values(6,301,302,500);
commit;

drop table temp_src
create table temp_src
(
id number,
from_temp number,
to_temp number,
weight_temp number
)
commit;
insert into temp_src values(1,100,101,200);
insert into temp_src values(2,200,201,200);
insert into temp_src values(4,300,301,300);
insert into temp_src values(5,300,302,400);
insert into temp_src values(6,301,302,600);

commit;

SELECT from_src, to_src,weight_src
FROM grath_src
UNION all
SELECT from_temp, to_temp,weight_temp
FROM temp_src
minus
SELECT from_src, to_src, weight_src
FROM grath_src 
INNER JOIN temp_src  ON grath_src.from_src=temp_src.from_temp  and
 grath_src.to_src=temp_src.to_temp
