/*
��������� ��������, ������� ���������� ������� � ������ -  �96,300,799�
*/
drop table numbers;
create table numbers
(
num number
);
commit;
insert into numbers values(100);
insert into numbers values(96);
insert into numbers values(500);
insert into numbers values(987);
insert into numbers values(799);
insert into numbers values(80);
insert into numbers values(300);
insert into numbers values(657);
commit;
SELECT num
FROM numbers 
WHERE num NOT IN (SELECT regexp_substr(str, '[^,]+', 1, LEVEL)
  FROM (SELECT '96,300,799' str FROM dual)
CONNECT BY instr(str, ',', 1, LEVEL-1) > 0); 
